"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const usersSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    rut: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    celular: {
        type: String,
        require: true,
        trim: true
    }
});
exports.default = mongoose_1.model('Indicador', usersSchema);
