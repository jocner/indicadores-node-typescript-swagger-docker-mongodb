"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.indicadorFecha = exports.indicador = void 0;
const axios_1 = __importDefault(require("axios"));
const indicador = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const indicador = yield (yield axios_1.default.get('https://mindicador.cl/api')).data;
        console.log("resultado", indicador);
        res.json(indicador);
    }
    catch (err) {
        console.log(err);
    }
});
exports.indicador = indicador;
const indicadorFecha = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { tipo_indicador, fecha } = req.body;
        const indicador = yield (yield axios_1.default.post(`https://mindicador.cl/api/${tipo_indicador}/${fecha}`)).data;
        console.log("info", tipo_indicador, fecha);
        console.log("resultado", indicador);
        res.json({ indicador });
    }
    catch (err) {
        console.log(err);
    }
});
exports.indicadorFecha = indicadorFecha;
