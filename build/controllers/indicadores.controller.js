"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUser = exports.deleteUser = exports.getUser = exports.indicador = exports.getUsers = exports.postUser = void 0;
const User_1 = __importDefault(require("../models/User"));
const axios_1 = __importDefault(require("axios"));
// const Product = require('../models/product');
const postUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { email, rut } = req.body;
        let users = yield User_1.default.findOne({ email: email });
        if (users) {
            res.status(402).json({ msg: 'ya existe un usuario' });
        }
        if (!email || !rut) {
            console.log("req vacia", email);
            res.status(404).json({ msg: 'error en el servicio' });
        }
        else {
            const user = new User_1.default(req.body);
            // const user = new User(newUser);
            yield user.save();
            res.json({ user });
        }
    }
    catch (err) {
        console.log(err);
    }
});
exports.postUser = postUser;
const getUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // const indicador = await axios.get('https://mindicador.cl/api');
        const indicador = 'https://mindicador.cl/api';
        const users = yield User_1.default.find();
        console.log("resultado", indicador);
        res.json({ users });
    }
    catch (err) {
        console.log(err);
    }
});
exports.getUsers = getUsers;
const indicador = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const indicador = axios_1.default.get('https://mindicador.cl/api');
        const users = yield User_1.default.find();
        console.log("resultado", indicador);
        res.json({ users });
    }
    catch (err) {
        console.log(err);
    }
});
exports.indicador = indicador;
const getUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const user = yield User_1.default.findById(id);
        // if (!user) {
        //  res.status(404).json({msg: 'no existe este usuario'});
        // }
        res.json({ user });
    }
    catch (err) {
        console.log(err);
    }
});
exports.getUser = getUser;
const deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const user = yield User_1.default.findByIdAndDelete(id);
        // if (!user) {
        //  res.status(404).json({msg: 'no existe este usuario'});
        // }
        res.json({ user });
    }
    catch (err) {
        console.log(err);
    }
});
exports.deleteUser = deleteUser;
const updateUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const { email, celular } = req.body;
        const user = yield User_1.default.findByIdAndUpdate(id, { email, celular });
        res.json({ user });
    }
    catch (err) {
        console.log(err);
    }
});
exports.updateUser = updateUser;
