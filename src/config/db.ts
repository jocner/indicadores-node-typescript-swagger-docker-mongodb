import { connect } from 'mongoose';
//const mongoose = require('mongoose');
// require('dotenv').config({ path: '../variables.env'});
import * as dotenv from 'dotenv';

dotenv.config({ path: '../variables.env' });

const DB_MONGO = 'mongodb+srv://jocner:jocner@cluster0.axw6l.mongodb.net/swagger-typescript';

//const {  } = require('dotenv').config({ path: '../variables.env'});


// const conectarDB = async() => {
//     try {
    
//         await connect( process.env. , {
//            useNewUrlParser: true,
//            useUnifiedTopology: true,
//            useFindAndModify: false
//         });
//         console.log('DB Conectada');
//     } catch(error) {
//         console.log('hubo un error');
//         console.log(error);
//         process.exit(1);
//     }
// }

const conectarDB = async() => {
    try {
    
        await connect(DB_MONGO , {
           useNewUrlParser: true,
           useUnifiedTopology: true,
           useFindAndModify: false
        });
        console.log('DB Conectada');
    } catch(error) {
        console.log('hubo un error');
        console.log(error);
        process.exit(1);
    }
}

//connect(process.env.DB_MONGO)


// module.exports = conectarDB;

export default conectarDB;


